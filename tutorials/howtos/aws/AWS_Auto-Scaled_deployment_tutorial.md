# Auto-Scaling Open edX AWS Deployment Tutorial

Purpose: This tutorial will walk you through deploying edX to [Amazon Web Services](https://aws.amazon.com) and set up AutoScaling such that new instances are automatically launched and shut down based on demand.

We will be using [Ansible](https://www.ansible.com/) to automate
deployment. We will be using external MySQL ([RDS](https://aws.amazon.com/rds)),
MongoDB ([Atlas](https://www.mongodb.com/cloud)) databases, and will
use Elasticache and Elasticsearch provided by AWS in order to provide
better scalability and fault tolerance. This ensures that our instance
is mostly stateless (other than logs, etc).

<br><br><br>

--------------

**TODO**: These instructions need to be replaced with Terraform code templates. All new AWS infrastructure should be defined and managed by Terraform.

Do not manually provision any new servers using the steps below! Instead please do it using Terraform and then replace these instructions with the new Terraform-based instructions. If you don't have time/budget to fully use Terraform, then please at least partially use Terraform and make sure each new deployment is using more Terraform than the last. See [the Terraformed Infrastructure epic](https://tasks.opencraft.com/browse/SE-2436) for details.

--------------

<br><br><br>

Auto-scaling on AWS involves a number of components that work together to
define how new instances are launched and terminated. It is possible to
configure a minimum, a maximum and a desired number of instances, and to
specify the exact conditions under which a new instance will be launched
or shut down. You also need to specify what instance it is that needs to
be launched.

You will need to create three things in order to set up auto-scaling.
- An AMI for our 'edx-app' instance
- A Launch Template
- An Auto Scaling Group

Most of the process here is the same as that for [an AWS Multi-Instance
Deployment](AWS_ELB_deployment_tutorial.md). You can follow all the steps
from that document, except for the part where you launch a second 'edx-app'
instance, since we will be using AWS's auto-scaling mechanisms to handle
that here.

The end result of following [the AWS Multi-Instance Deployment
guide](AWS_ELB_deployment_tutorial.md) should be that you now have an AMI
that has the 'edx-app' set up and ready to run, and you have load-balancer
set up to handle multiple instances.

Please check the [Consul configuration section](./AWS_deployment_tutorial.md#consul-configuration) in the AWS deployment guide to ensure that consul works properly when instances are created using the AMI specified in the launch template.

Creating a Launch Template
--------------------------

When we launch a new instance on EC2 there are a number of details that
we provide, such as the AMI, instance type, key pair, networking settings,
storage settings, security groups etc. When AWS is handling the launch of
new instances it needs us to provide all this data in advance; this is
where a Launch Template comes in.

As part of following the instructions the Multi-Instance Deployment, you
would have created a new instance, from which you created the AMI. The
simplest way to create a Launch Template is to right click that instance
and select *Create Template from Instance*. Go through the settings here
and make sure they make sense.

Creating an Auto-Scaling Group
------------------------------

The next step is to create a new Auto-Scaling Group for this template.

To do so, visit the *Auto Scaling Groups* page on the EC2 panel. It will
be on the left sidebar, under the *AUTO SCALING() section.

In the wizard here, select *Launch Template* and select the new template
we just created above, and click next.

You will now be able to give this group a name. Give it a good descriptive
name, and fill in appropriate settings for the group size, network and
subnet based on your load-balancer configuration. For *Launch Template
Version* select *Default*, this will make upgrades slightly easier.

In the next step you can set up scaling policies. You should select the
option to use scaling policies to adjust the capacity of the group.
Set reasonable values for the minimum and maximum group size based on
the client's demands or requirements. The actual scaling policy may need
fine-tuning. For instance, you could set the group to scale based on an
"Average CPU Utilization" of 50% or above. 300 seconds is a reasonable
warm-up period for a newly launched instance.

In the next steps you can also set up notifications and tags. Set
notifications as desired. A good policy is to set the Name tag to the
current launch generation, i.e. oc-edxapp-22 for the 22nd redeployment.

The final step will ask you to review the configuration and finally the
auto scaling group is created.

Our work is not done though. We still need to configure this to work
with the load balancer! To do that you need to edit the newly created
*Auto Scaling Group* and specify a target group, setting it to the
target group created for the Load Balancer. You would have created
one when following the Multi-Instance Deployment guide.

With the target group set, our auto-scaling instance is now deployed!

Upgrading Instances with Auto-Scaling Setup
-------------------------------------------

Upgrading instances has its challenges when using an auto-scaling
setup, especially when we perform a major upgrade.

The process for upgrades is very similar to the process above. Launch
a new single instance as you would for a simple deployment, and use it
to create a new AMI.

Create a new *Launch Template*, but this time choose to create a new
version of the previous template. This will keep all the old settings,
but will use the new AMI.

Now you can go back and edit our *Auto Scaling Group* and select this
new template version. Temporarily increase the minimum capacity to
double of what is usually required. Doing so will force AWS to launch
new instances based on the new template that uses the latest AMI with
our patched or upgraded release.

Once AWS has launched all the new instances, you can reduce the desired
and minimum capacity down to its usual value. This will make AWS
terminate the instances based on the older launch template based on the
older AMI. We will now have all new and upgraded instances.
