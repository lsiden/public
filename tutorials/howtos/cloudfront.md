# Serving static files from edx-platform via CloudFront
## S3
1. Create new S3 bucket, e.g. `oc-edxapp-prod-static`.
2. In the new bucket go to `Permissions -> CORS configuration` and set the following configuration:
	```xml
	<?xml version="1.0" encoding="UTF-8"?>
	<CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
	<CORSRule>
	    <AllowedOrigin>*</AllowedOrigin>
	    <AllowedMethod>GET</AllowedMethod>
	    <AllowedMethod>HEAD</AllowedMethod>
	    <AllowedHeader>*</AllowedHeader>
	</CORSRule>
	</CORSConfiguration>
	```
1. Go to `Management -> Lifecycle` and create new rule with the following parameters:
	1. Name: `Cleanup`.
	1. Rule scope: `Apply to all objects in the bucket`.
	1. Configure expiration:
		1. Current version: `182` days (or a different value, depending on how often you want to clean these up). We can do this, becuase each static file has its hash updated when its content changes.
		1. Previous versions: `14` days.
		1. Incomplete multipart (optional): `14` days.
	1. Check `I acknowledge that this lifecycle rule will apply to all objects in the bucket.`.

## IAM
1. Go to `IAM -> Policies` and create a new one with the following `JSON` (you can call the policy `oc-edxapp-prod-static-s3-access`):
	```json
	{
	    "Version": "2012-10-17",
	    "Statement": [
		{
		    "Effect": "Allow",
		    "Action": "s3:*",
		    "Resource": [
			"arn:aws:s3:::oc-edxapp-prod-static",
			"arn:aws:s3:::oc-edxapp-prod-static/*"
		    ]
		}
	    ]
	}
	```
1. Go to `IAM -> Users` and create a new one with the following options:
	1. User name: `oc-edxapp-prod-static-s3`.
	1. Access type: `Programmatic access`.
1. Select `Attach exisitng policies directly` and find the one you created (`oc-edxapp-prod-static-s3-access`).
1. Save the access key and secret key. You will need it later.

## CloudFront
1. Go to CloudFrtont and create new distribution with the following parameters:
	1. Delivery method: `Web`.
	1. Origin Domain Name: `oc-edxapp-prod-static.s3.amazonaws.com`.
	1. Origin Path: leave this empty unless you want to organize S3 bucket differently.
	1. Restrict Bucket Access: `Yes`.
	1. Origin Access Identity: `Create a New Identity`.
	1. Comment: `oc-edxapp-prod-static-s3-cloudfront-access-identity`.
	1. Grant Read Permissions on Bucket: `Yes, Update Bucket Policy`.
	1. Viewer Protocol Policy: `Redirect HTTP to HTTPS`.
	1. Allowed HTTP Methods: `GET, HEAD`.
	1. Cache and origin request settings: `Use a cache policy and origin request policy`.
	1. Cache Policy -> `Create a new policy`:
		1. Name: `oc-edxapp-static-cache-policy`.
		1. Minimum TTL: `31536000`.
		1. Maximum TTL: `31536000`.
		1. Default TTL: `31536000`.
		1. Headers: `Whitelist` and choose the following headers:
			1. `Access-Control-Request-Method`.
			1.` Access-Control-Request-Headers`.
			1. `Origin`.
		1. Check `Cache gzip objects`.
	1. Click on the "refresh" icon and choose newly created policy (`oc-edxapp-static-cache-policy`).
	1. Origin Request Policy: `Managed-CORS-S3Origin`.
	1. Compress Objects Automatically: `Yes`.
	1. SSL Certificate: `Default CloudFront Certificate (*.cloudfront.net)`.
	1. Comment: `oc-edxapp-prod static`.

## configuration-secure
1. Add the following variables:
	```yaml
	# S3 with static files (used by CloudFront)
	CLOUDFRONT_S3_ACCESS_KEY_ID: access_key from IAM
	CLOUDFRONT_S3_SECRET_ACCESS_KEY: secret_key from IAM
	CLOUDFRONT_S3_BUCKET_NAME: oc-edxapp-prod-static
	# CloudFront CDN
	EDXAPP_STATIC_URL_BASE: https://DOMAIN_NAME.cloudfront.net/static/
	CUSTOM_EDXAPP_ADDITIONAL_CRON_JOBS:
	  - name: Upload static files to S3
	    user: "{{ edxapp_user }}"
	    job: "AWS_ACCESS_KEY_ID={{ CLOUDFRONT_S3_ACCESS_KEY_ID }} AWS_SECRET_ACCESS_KEY={{ CLOUDFRONT_S3_SECRET_ACCESS_KEY }} /usr/local/bin/aws s3 sync --cache-control 'public, immutable, max-age=31536000' {{ edxapp_staticfile_dir }} s3://{{ CLOUDFRONT_S3_BUCKET_NAME }}/static/"
	    special_time: reboot
	EDXAPP_ADDITIONAL_CRON_JOBS: '{{ COMMMON_EDXAPP_ADDITIONAL_CRON_JOBS + CUSTOM_EDXAPP_ADDITIONAL_CRON_JOBS }}'
	```

If the client uses Continuous Delivery, then it can be combined with the pipelines. Uploading static files after reboot is just a workaround, but it works well with `sync`.