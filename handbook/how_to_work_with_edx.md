# How to work with edX

We work with edX all the time, in various ways - but this document is specifically about how to work with edX _as a client_, when they're paying us for the work. edX calls this "**Blended Development**", because the team is a blend of OpenCraft and edX people.

An overview of when, why, and how edX does "blended development" is on their wiki at [Blended Development Runbook and Best Practices](https://openedx.atlassian.net/wiki/spaces/COMM/pages/1863189038/Blended+Development+Runbook+and+Best+Practices).

## Links

* [edX Jira](https://openedx.atlassian.net/jira)
* [edX wiki](https://openedx.atlassian.net/wiki/home)
* [Blended PR Workflow](https://openedx.atlassian.net/wiki/spaces/COMM/pages/1947926666/Blended+PR+Workflow)

## General tips:

* Before opening a [pull request](pull_request_process.md), review the [Blended PR Workflow](https://openedx.atlassian.net/wiki/spaces/COMM/pages/1947926666/Blended+PR+Workflow) which explains the procedure for opening a PR on any blended project. (In addition to the usual [PR process](https://handbook.opencraft.com/en/latest/pull_request_process/) that we follow on all projects.)
* Remember to default to public communications. This is an open source project and generally most of the aspects of blended development can be public - so default to discussing and documenting things on public Confluence pages, Jira tickets, ADRs, Discourse threads and public Slack channels.
    * If that doesn't get enough attention or timely response, then ping on synchronous and/or private tools, linking to the public question.
* Slack is one of edX's major communication channels and we should not be hesitant to ping people on it, in the same way we try to reduce synchronous pings on Mattermost within OpenCraft.
    * Note: understand that edX has two Slacks: their internal Slack (which we cannot access directly), and the Open edX Slack (which we do have access to). Most edX employees are on both, and some channels are shared between both Slacks, such as [the #edx-shared-opencraft channel](https://openedx.slack.com/archives/C01ATGRBBDM).
    * Some projects we do with edX have their own public Slack channel on the Open edX Slack channel, which should be documented on the project's confluence page.
    * If you don't have access to [the #edx-shared-opencraft channel](https://openedx.slack.com/archives/C01ATGRBBDM), ask someone to invite you. It is OK to share some non-public information on this channel, as it is private to OpenCraft and edX.
    * Slack should be treated as ephemeral, so if it's used for making any decisions impacting the project, those decisions should be documented in Confluence right away.
* Iterative development and regularly shipping PRs to production is the expectation; if your PR is languishing without a review for more than a week, please considering that a big problem that you need to solve immediately. Ping people and escalate as needed.
* If newcomers are working on tickets for edX, they should first submit their PR to an internal fork (OpenCraft fork) and get it reviewed, and only submit it upstream once the reviewer has approved it.

## How to escalate and issue / how edX will escalate an issue

If you are blocked or have an urgent problem (or edX is in that situation), the procedures for escalating issues with each other are documented in Confluence at [Blended Provider Status Pages: OpenCraft: Escalation Path](https://openedx.atlassian.net/wiki/spaces/COMM/pages/1529676506/OpenCraft) ; also see the project page for each blended project, to see who the "edX lead" is as well as any project-specific escalation instructions.

## How to mention people on the edX Jira

Due to [an annoying bug](https://jira.atlassian.com/browse/JSDCLOUD-1476), you may find that you are not able to ping people on the [edX Jira](https://openedx.atlassian.net/jira). This is a very inconvenient situation, but there is a workaround for now:

1. [Find](https://openedx.atlassian.net/wiki/people/search) a profile page of a user you want to ping. Get the user ID - it’s in the URL, after `/people/`.
1. Go to [this page](https://openedx.atlassian.net/secure/ViewPersonalSettings.jspa) and turn off “Jira labs” at the bottom.
1. Add comment in Jira by using `[~accountid:557058:931b5939-e9eb-4c44-b93d-4892f3d357bd]` to ping a user. This example shows how to ping Marco, so you might want to replace the characters after `accountid:`.

Some people you may be trying to mention:

* `[~accountid:557058:931b5939-e9eb-4c44-b93d-4892f3d357bd]` Marco (Product)
* `[~accountid:557058:e9f278cc-7036-49ad-ba9e-e73ae3e81bdf]` Natalia (Open Source)
