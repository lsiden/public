# Sprint planning agenda

## Sprint planning

Planning for a given sprint is done asynchronously, with the steps happening during the previous sprint. So, in any given sprint, one of the tasks everyone has is to plan for the following sprint.

The steps involved in that planning are described in the [sprint planning checklist template](https://view.monday.com/942397140-b2d0d779a236cf486837f8c7391a08d9), as well as the follow-up tasks to be done during the sprint itself.

Our [bot](https://gitlab.com/opencraft/dev/crafty-bot/) creates a dedicated checklist for each member, every sprint, in the workspace which corresponds to their cell. The checklist is to be completed by all team members during the sprint, according to the timelines described there.

## Tentative assignees

To ensure that tasks lined up for the sprint are all actively being taken care of, from the beginning of sprint planning, some tasks are tentatively assigned by their creator. To show the tentative assignment, these tasks are flagged in Jira (they appear in yellow in the backlog).

The tentative assignee isn’t meant to be necessarily the person who will be actually working on the task. Like for PR reviews, the goal is to ensure proper focus and avoid dilution of responsibility, so we need one specific person assigned to each ticket to do that review early on, even if the actual assignee changes later.

The assignment is done without considering size/availability, since the tickets aren’t yet scoped or estimated. The tentative assignee is merely the person who will be responsible for finding the final assignee, and getting the task through the planning steps in the meantime, such as reviewing the task description.

The tentative assignee is responsible for reassigning and/or rescoping the tickets before the sprint starts. Note that if no action is taken, by default the tentative assignee will become the actual task assignee when the sprint starts.

## Estimation session

The sprint planning manager creates the sprint estimation session in Jira. An email with a link is sent out when the session opens. For each task:

* Read through the description
* Decide whether you can take it
* If you do want it, it's ok to spend a bit more time investigating
* If you have questions, put them in comments on the ticket
* Assign points as described in the [process section](process.md)
* Use the "estimate comments" box to qualify estimates
* Choose `?` if you have no idea how many points
* A little green `(/)` will appear next to your picture up top when you are done

Complete the estimation session by Friday. Stay conscious of time budgets and try not to log more than a few minutes per task to do these estimates.

## Task insertion

The following process applies when someone wants to add a ticket to the sprint after the Thursday EOD deadline:

* If the ticket replaces another ticket already in the sprint, or is split out from one: explicitly reference the ticket being replaced or split out from in the task description, and ping the sprint planning manager for confirmation.
* Otherwise, place the ticket at the top of the stretch goals, and ping the sprint planning manager to inform of the desire to include the ticket in the sprint if possible. There would not be any guarantee of such an inclusion, but once the sprint is fully ready (ie, all the tickets in the sprint are ready and all the other verifications before the start of the sprint have been completed, including ensuring nobody is overcommitted), then the ticket can be considered for a sprint insertion if someone still needs work. This is handled the same way we would treat a ticket that we want to add in the middle of a sprint.
* No other exception. A sprint insertion has to follow either of these paths, or it will be moved to the next sprint or the stretch goals.

## Sprint video update

The video updates are a way for us to keep a regular occasion for everyone in a cell to see & hear each other directly on video, rather than switch to 100% textual. They also include an optional text component, that completes the video format as necessary. The updates are posted in a dedicated "Sprint updates" forum thread for the cell, reusing it between sprints.

The update should contain a video recording of 1 to 2 minutes maximum on [Loom](https://www.loom.com/) ([credentials](https://vault.opencraft.com:8200/ui/vault/secrets/secret/show/shared/www.loom.com)) or using the software of your choice such as [OBS Studio](https://drive.google.com/file/d/1NnAfy0_Iskzfs_1O_D6IsFL9laG9Ey7C/view)

* Share **both** your screen & camera, and name the video `Sprint X - <Cell> - <Your name>` with X being the number of the sprint finishing
* If you use a Loom video, copy the link from the "Send your video" box on the top right of the Loom video page, and [place it on a line by itself to ensure discourse shows the video inline](https://forum.opencraft.com/t/sprint-updates-serenity/634/9).
* If you create your own video, upload it to the [Async planning meeting videos folder](https://drive.google.com/drive/folders/19L9MDRnfUpUxpIzYbcowiqYg-0ahgJEU), and [use an iframe to display the video inline in your post](https://forum.opencraft.com/t/sprint-updates-serenity/634/11):
  * `<iframe src="https://drive.google.com/file/d/[hash]/preview" width="640" height="360" frameborder="0"></iframe>`
  * Note that it might be necessary to disable the enhanced tracking protection from Firefox on `forum.opencraft.com` to play videos embedded this way.
* [Optional] Include a text update below your video - for example to highlight points that shouldn't be missed, or provide links to elements you mention in your video.

Present the following points in your update:

* If a newcomer is scheduled to join this week, ensure you've created a quick introductory video of yourself and stored it under your cell in [OpenCraft Shared Drive > Meeting videos > Team introductions](https://drive.google.com/drive/u/2/folders/1ryyhg5J51MesV1FpbYyxMZLk0njYIo-M). Provide your name, how long you have been with OpenCraft, your current location, and an interesting fact about you.
* Some of your work, in a mini-demo/presentation of what you did during the previous sprint, mentioning any major accomplishments, interesting tickets, lessons learned, or problems encountered that are worth sharing with the team. What has been accomplished, what went well, what went not so well, and the reason for any spillover. Don't just show your Jira board though -- people can already go see this by themselves.
* Any upcoming vacation if you have some in the upcoming sprint.
* [Optional] Giving kudos
* [Optional] Pointing to announcements and discussion started on the forum, introducing them orally when it’s useful.
* [Optional] If you would like to make an extended presentation or tech demo, which doesn't fit in the 2 minutes of your weekly update, record a separate second video, which you will post in the forum in a dedicated thread. Mention who the intended audience is (the rest of your cell, the whole company, etc.), ask for questions explicitly and come back to the thread regularly to answer them.
* The sprint firefighter with the rotation on the first week of the upcoming sprint reminds of the theme and time of the social chat they organize this week

## Social chat

With the move to asynchronous planning meetings, we don't get to interact with each other in videocalls regularly anymore! To keep an occasion to do that, and actually make those interactions nicer, we are setting up regular _social chats_. They are a less formal and utilitarian setting, and are there more to interact with each other in an informal setting, and are optional.

* They are organized by rotation, and happen every sprint during the first week - every core team member organizes one in turn for their cell, and open to anyone from any cell. We use the firefighter rotation for this, the firefighter for the first week of a sprint posts a topic of their choice and a time (during that same first week of the sprint).
* Any topic goes, be creative! It doesn't need to be about work (shouldn't be about work?): games, teaching how to prepare a cocktail or recipe, participants showing an interesting place in their neighborhood, etc.
* The organizer describes the event in a dedicated "Social chat" forum thread for the cell, reused each sprint.
* The organizer creates a calendar event on the cell's calendar at the time of their choice, and invites all the cell's members as optional attendees. The calendar invite should include:
  * The description/agenda of the event
  * A Zoom meeting URL
  * A link to the corresponding forum post
  * A link to the task where to log the meeting time
  * A mention that the meeting is optional (nobody except the organizer should feel forced to attend, and due to timezones & schedules not everyone will be able to attend every meeting)
* The 30 minutes are logged on a dedicated task by participants. The organizer creates the task dedicated to a specific meeting when organizing it, including it on a "Social chat" epic, with the "Meetings" account set.
* Participants can stay longer than 30 minutes at the event if they want to, or attend multiple events from multiple cells, but the time logged is timeboxed to 30 minutes overall. Anyone can leave at any time.

## All-hands meetings and tech talks

From time to time we get together and do tech talks, to share knowledge and as an opportunity to meet members of the other cells. Note that a tech talk can also be contributed asynchronously using the video recording at the end of any sprint.

An all-hands meeting will be scheduled when there's something we want to discuss at the team level, or when there's a tech talk. To schedule a tech talk, create a ticket in your own cell to prepare it, then discuss in [the tech talks thread](https://forum.opencraft.com/t/all-hands-meetings-and-tech-talks/189/10) a date that works for all cells (it must be on the same day as a mid-sprint meeting).
