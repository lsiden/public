# public-doc

Public documentation repo for the OpenCraft handbook

See [handbook.opencraft.com](https://handbook.opencraft.com/) for the latest release.

This handbook is build using the static site generator [mkdocs](https://www.mkdocs.org/).

To build the docs to HTML, create a python 3.8 virtual environment.

```bash
virtualenv venvs/docs -p python3.8
source venvs/docs/bin/activate
```

Then, run from the root repository directory:

```bash
make install_prereqs
make compile
```

...or, to do a full installation, validation and build all in one go, do:

```bash
make
```

After the first build, to serve the docs locally, run from the root repository directory:

```bash
make run
```

Hooks for mkdocs are stored in the `hooks` directory. This includes python code that should be checked.
To check this code, run:

```bash
make quality
```

# Technical documentation

In February 2019 the technical documentation from this repository were
moved to a [dedicated
repository](https://gitlab.com/opencraft/documentation/tech).

# Read the Docs

* Configured with readthedocs.yml
* Account information [in Vault](https://vault.opencraft.com:8200/ui/vault/secrets/secret/show/core/OpenCraft Tools : Resources : Read the Docs)
* GitLab integration so the doc is rebuilt every time a commit is pushed in master is in the [settings of the public repository](https://gitlab.com/opencraft/documentation/public/settings/integrations)
