"""
Type annotations used primarily for validating YAML.
"""

from typing import TypedDict, List


class Member(TypedDict):
    """
    All cell members will be expected to have these fields. This model may get more complex depending on how
    """
    name: str
    email: str


class TitledMember(Member):
    """
    Member with a specifically displayed title. Used for the MetaCell.
    """
    title: str


class ToolKit(TypedDict):
    """
    Links to standard tools each cell should have. These can be empty strings if the tools do not exist yet.
    """
    chat: str
    sprint_board: str
    backlog: str
    epics_board: str


class Roles(TypedDict):
    """
    Standard roles. These can be empty strings if no one is assigned yet. Each entry should be an email,
    which will be checked against the cell's team to verify that the cell member exists.
    """
    recruitment_manager: str
    sprint_manager: str
    sprint_planning_manager: str
    epic_manager: str
    sustainability_manager: str
    ospr_liaison: str
    official_forum_moderator: str
    devops_specialist: str


class MetaRoles(TypedDict):
    """
    Roles for the metacell.
    """
    official_forum_moderator: str
    community_liaison: str


class Cell(TypedDict):
    """
    Defines information about a cell.
    """
    name: str
    members: List[Member]
    tools: ToolKit
    roles: Roles
    rules: List[str]


class MetaCell(TypedDict):
    """
    Structure for the 'Meta-cell', or where non-team members go.
    """
    members: List[TitledMember]
    roles: MetaRoles


class OpenCraft(TypedDict):
    """
    Defines information about the entire organization.
    """
    cells: List[Cell]
    meta_cell: MetaCell
