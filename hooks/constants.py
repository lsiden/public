"""
Constants used by the build hooks.
"""
from pathlib import Path

ROOT = Path(__file__).parent.parent
HOOKS = ROOT / 'hooks'
TEMPLATES_PATH = HOOKS / 'templates'
DATA_PATH = HOOKS / 'data'
TEAM_FILE_PATH = DATA_PATH / 'opencraft.yml'
CELL_TEMPLATE = 'cells.md.jinja'
CELL_OUTPUT_PATH = ROOT / 'handbook' / 'cells.md'
